package net.jcornelioguzman.projects.numbertoword.util;

/**
 * @author Juan Cornelio Guzman
 * @since December 8, 2015
 */
public final class ScaleConstants {

	private ScaleConstants() {
		// This will prevent instantiation of helper class
	}

	public static final String HUNDRED = " HUNDRED ";

	public static final String THOUSAND = " THOUSAND ";

	public static final String MILLION = " MILLION ";

	public static final String BILLION = " BILLION ";
}
